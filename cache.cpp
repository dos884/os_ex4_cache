//
// Created by asafbe on 5/20/16.
//

//**Defines**//
#include <iostream>
#include <list>
#include <map>
#include <malloc.h>
#include <algorithm>
#include <unistd.h>
#include <string.h>
#include <sstream>
#include "cache.h"
#define INIT_REF_COUNT 1

using namespace std;
enum Lst_type{NEW, MID, OLD};
typedef struct node_info
{
    int block_num;
    int ref_count = INIT_REF_COUNT;
    Lst_type type = NEW;
    list<node_info*>* lst_ptr;
    string path;
    char* data = NULL;
}node_info;

//A comparison operator for the multimap.
struct str_cmp {
    bool operator()(const string s1, const string s2) const
    {
        return (s1) < (s2);
    }
};
typedef map<string, list<node_info*>*, str_cmp> MAP_PATH_TO_BLOCK_PTR;
typedef list<node_info*> NEW_CACHE_PARTITION_LST;
typedef list<node_info*> MID_CACHE_PARTITION_LST;
typedef list<node_info*> OLD_CACHE_PARTITION_LST;
typedef list<node_info*> CACHE_PARTITION_LST;
typedef map<string, list<node_info*>*>::iterator MAP_PATH_TO_BLOCK_ITR;

//**Globals**//
MAP_PATH_TO_BLOCK_PTR path_lst_ptr_map;
NEW_CACHE_PARTITION_LST lst_new_cache;
MID_CACHE_PARTITION_LST lst_mid_cache;
OLD_CACHE_PARTITION_LST lst_old_cache;

unsigned int lst_new_cache_size;
unsigned lst_mid_cache_size;
unsigned lst_old_cache_size;

int global_file_handle = -1;

__blksize_t blk_size;

int numberOfBlocks;
float fOld;
float fNew;

//**Helper Functions**//
size_t next_multiple(size_t base, size_t block_size)
{
    size_t div = base/block_size;
    if(base % block_size > 0) {
        div++;
    }
    return block_size * div;
}

list<int>* return_found_cash_blocks(string path)
{
    list<int> *lst_found_blocks_nums = new list<int>();

//    for (CACHE_PARTITION_LST::const_iterator it = fh_lst_ptr_map[path] -> begin(),
//                 end = fh_lst_ptr_map[path] -> end(); it != end; ++it) {
//
//        lst_found_blocks_nums->push_back((*it) -> block_num);
//    }
    for(auto it = path_lst_ptr_map[path] -> begin(); it != path_lst_ptr_map[path] -> end(); it++)
    {
        lst_found_blocks_nums->push_back((*it)->block_num);
        //DEBUG_PRINT("Inside found_list:" << (*it)->block_num)
    }

    return lst_found_blocks_nums;
}

bool is_block_in_lst(int block_num, list<int> *lst_found_blocks_nums)
{
//     std::find((*lst_found_blocks_nums).begin(), (*lst_found_blocks_nums).end(), block_num);
//    return ( != (*lst_found_blocks_nums).end());
    bool res = false;
    for (auto it = lst_found_blocks_nums->begin(),
                 end = lst_found_blocks_nums->end(); it != end; ++it)
    {
        if((*it) == block_num)
        {
            res = true;
            //DEBUG_PRINT("Block #" << block_num << " is indeed in list!")
        }
    }
    return res;
}

//From map.
node_info* get_block_ptr(string path, int block_index)
{
    node_info* block_ptr = NULL;

    for (list<node_info *>::const_iterator it = path_lst_ptr_map[path]->begin(),
                 end = path_lst_ptr_map[path]->end();
         it != end; ++it)
    {

        node_info* current = (*it);

        //DEBUG_PRINT("Trying to match with: " << current -> block_num)

        if(current->block_num == block_index)
        {
            block_ptr = (*it);
        }
    }

    return block_ptr;
}

CACHE_PARTITION_LST::iterator ret_block_iterator(node_info* block_ptr)
{
    //CACHE_PARTITION_LST::const_iterator it;
    CACHE_PARTITION_LST* lst_ptr = (*block_ptr).lst_ptr;

//    switch(block_ptr -> type)
//    {
//        case NEW:
//            lst_ptr = &lst_new_cache;
//            break;
//        case MID:
//            lst_ptr = &lst_mid_cache;
//            break;
//        case OLD:
//            lst_ptr = &lst_old_cache;
//            break;
//        default:
//            DEBUG_PRINT("Error getting list type from block")
//    }

    return std::find((*lst_ptr).begin(), (*lst_ptr).end(), block_ptr);
//    if(it != (*lst_ptr).end()) //If there was a match.
//    {
//        return it;
//    }
//
//    return NULL;
}

//std::list::sort is stable (standard, section 23.2.24).
void lst_sort_by_ref_count(CACHE_PARTITION_LST *lst)
{
    (*lst).sort([](const node_info *a, const node_info *b)
                {
                    return (*a).ref_count > (*b).ref_count;
                });
}

//int cpy_node_info(node_info* dst, node_info* src)
//{
//    dst->block_num = src->block_num;
//    dst->ref_count = src->ref_count;
//    dst->type = src->type;
//    dst->lst_ptr = src->lst_ptr;
//    dst->data = src->data;
//}

void update_cache_existing_block(node_info* block_ptr)
{
    node_info* tmp_block_ptr, *tmp_new_lst_front, *tmp_mid_lst_front;
    //Todo: Free memory of 'new node_info'.

    if(block_ptr -> type != NEW) //MID or OLD.
    {
        block_ptr -> ref_count++;
    }

    CACHE_PARTITION_LST::iterator block_it = ret_block_iterator(block_ptr);

    if((block_it != ((*block_it) -> lst_ptr -> end()))) //If node found.
    {
        if ((lst_new_cache_size >= (lst_new_cache.size() + 1)) ||
            (lst_new_cache_size == lst_new_cache.size() &&
             block_ptr->type == NEW)) //Free space or Reposition NEW element.
        {
            tmp_block_ptr = *block_it; //Get ptr.
            (*block_it) -> lst_ptr -> erase(block_it);
            lst_new_cache.push_back(tmp_block_ptr);

        } else if (lst_mid_cache_size >= (lst_mid_cache.size() + 1))
        {
            tmp_block_ptr = *block_it; //Get ptr.
            (*block_it) -> lst_ptr -> erase(block_it);
            tmp_new_lst_front = lst_new_cache.front();
            lst_new_cache.pop_front();

            tmp_block_ptr -> lst_ptr = &lst_new_cache;
            tmp_block_ptr -> type = NEW;
            lst_new_cache.push_back(tmp_block_ptr);

            tmp_new_lst_front -> lst_ptr = &lst_mid_cache;
            tmp_new_lst_front -> type = MID;
            lst_mid_cache.push_back(tmp_new_lst_front);

        } else {
            tmp_block_ptr = *block_it; //Get ptr.
            (*block_it) -> lst_ptr -> erase(block_it);

            tmp_new_lst_front = lst_new_cache.front();
            lst_new_cache.pop_front();

            tmp_mid_lst_front = lst_mid_cache.front();
            lst_mid_cache.pop_front();

            tmp_block_ptr -> lst_ptr = &lst_new_cache;
            tmp_block_ptr -> type = NEW;
            lst_new_cache.push_back(tmp_block_ptr);

            tmp_new_lst_front -> lst_ptr = &lst_mid_cache;
            tmp_new_lst_front -> type = MID;
            lst_mid_cache.push_back(tmp_new_lst_front);

            tmp_mid_lst_front -> lst_ptr = &lst_old_cache;
            tmp_mid_lst_front -> type = OLD;
            lst_old_cache.push_back(tmp_mid_lst_front);
        }
    }
//    cout << cache_to_string() << endl;
}

void update_cache_new_block(int block_index, string path, char *tmp_buf)
{
    node_info* block_ptr, *tmp_new_lst_front, *tmp_mid_lst_front, *tmp_old_lst_front;

    if(lst_new_cache_size >= (lst_new_cache.size() + 1)) //Can add new block.
    {
        block_ptr = new node_info;
        block_ptr -> block_num = block_index;
        block_ptr -> data = tmp_buf;
        block_ptr -> lst_ptr = &lst_new_cache;
        block_ptr -> path.assign(path);
        lst_new_cache.push_back(block_ptr);
        //Todo: Free memory of 'new node_info'.

    //Need to take out a block or/and rearrange cache.
    } else if (lst_mid_cache_size >= (lst_mid_cache.size() + 1))
    {
        tmp_new_lst_front = lst_new_cache.front();
        lst_new_cache.pop_front();

        block_ptr = new node_info;
        block_ptr -> block_num = block_index;
        block_ptr -> data = tmp_buf;
        block_ptr -> lst_ptr = &lst_new_cache;
        block_ptr -> path.assign(path);
        lst_new_cache.push_back(block_ptr);

        tmp_new_lst_front -> lst_ptr = &lst_mid_cache;
        tmp_new_lst_front -> type = MID;
        lst_mid_cache.push_back(tmp_new_lst_front);
        //Todo: Free memory of 'new node_info'.
    } else if(lst_old_cache_size >= (lst_old_cache.size() + 1))
    {
        tmp_new_lst_front = lst_new_cache.front();
        lst_new_cache.pop_front();

        tmp_mid_lst_front = lst_mid_cache.front();
        lst_mid_cache.pop_front();

        block_ptr = new node_info;
        block_ptr -> block_num = block_index;
        block_ptr -> data = tmp_buf;
        block_ptr -> lst_ptr = &lst_new_cache;
        block_ptr -> path.assign(path);
        lst_new_cache.push_back(block_ptr);

        tmp_new_lst_front -> lst_ptr = &lst_mid_cache;
        tmp_new_lst_front -> type = MID;
        lst_mid_cache.push_back(tmp_new_lst_front);

        tmp_mid_lst_front -> lst_ptr = &lst_old_cache;
        tmp_mid_lst_front -> type = OLD;
        lst_old_cache.push_back(tmp_mid_lst_front);
        //Todo: Free memory of 'new node_info'.
    } else { //All lists are full.
        tmp_new_lst_front = lst_new_cache.front();
        lst_new_cache.pop_front();

        tmp_mid_lst_front = lst_mid_cache.front();
        lst_mid_cache.pop_front();

        lst_sort_by_ref_count(&lst_old_cache); //Smallest ref-count last.
        tmp_old_lst_front = lst_old_cache.front();
        lst_old_cache.pop_front();

        block_ptr = new node_info;
        block_ptr -> block_num = block_index;
        block_ptr -> data = tmp_buf;
        block_ptr -> lst_ptr = &lst_old_cache;
        block_ptr -> path.assign(path);
        lst_new_cache.push_back(block_ptr);

        tmp_new_lst_front -> lst_ptr = &lst_mid_cache;
        tmp_new_lst_front -> type = MID;
        lst_mid_cache.push_back(tmp_new_lst_front);

        tmp_mid_lst_front -> lst_ptr = &lst_old_cache;
        tmp_mid_lst_front -> type = OLD;
        lst_old_cache.push_back(tmp_mid_lst_front);

        path_lst_ptr_map[path] -> remove(tmp_old_lst_front);
        delete[] (tmp_old_lst_front->data);
        //delete (tmp_old_lst_front->lst_ptr);
        delete(tmp_old_lst_front);
        //Todo: Free memory of 'new node_info'.
    }
    path_lst_ptr_map[path] -> push_back(block_ptr);
//    cout << cache_to_string() << endl;
}

//All of our block logic works with aligned buffer.
ssize_t retrive_all_info(string path, char *aligned_buf, size_t low_bound,
                         size_t up_bound)
{
    ssize_t retstat = 0, tmp_retstat = 0;
    //TODO : this is not required.. you get a rounded input always!
    list<int> *lst_found_blocks_nums;
    size_t total_mem_size = up_bound;
    size_t total_blocks_num;
    size_t start_block = (low_bound / (size_t) blk_size);
    //size_t addition_for_sub = 0;
    node_info* block_ptr;
    char *tmp_buf;

    total_blocks_num = (total_mem_size / (size_t) blk_size) + start_block;

    for(int i = (int)start_block; i < (int)total_blocks_num; i++)
    {
        //One block at a time.
        lst_found_blocks_nums = return_found_cash_blocks(path);
        if(is_block_in_lst(i, lst_found_blocks_nums))
        {
            block_ptr = get_block_ptr(path, i); //it val is the ptr.

//            int where = (i - (int)start_block) * blk_size;
//            char  * c_data= block_ptr -> data;
//            memcpy(&aligned_buf[where],c_data, (size_t) blk_size);

            memcpy(&aligned_buf[(i - (int)start_block) * blk_size], block_ptr -> data, (size_t) blk_size);
            retstat += (size_t) blk_size;
            /*int = */update_cache_existing_block(block_ptr);
        } else { //Need to bring block from fs.
            tmp_retstat = pread(global_file_handle, &aligned_buf[(i - (int)start_block) * blk_size], (size_t) blk_size, i * blk_size); //Update aligned buffer with the new data from the fs.

            if(tmp_retstat >= 0)
            {
                retstat += tmp_retstat;
            } else {
                return tmp_retstat;
            }

            tmp_buf = new char[(size_t) blk_size];
            memcpy(tmp_buf, &aligned_buf[(i - (int)start_block) * blk_size], (size_t) blk_size);
            /*int = */update_cache_new_block(i, path, tmp_buf);
            //Todo: delete[] tmp_buf;
        }
        delete(lst_found_blocks_nums);
    }

    return retstat;
}

void destroy_cache()
{
    //Todo: Free all memory from lists!.
    //Todo: Free block_ptr -> data from all nodes inside lists! (tmp_buf).

    for(MAP_PATH_TO_BLOCK_ITR map_it = path_lst_ptr_map.begin(); map_it != path_lst_ptr_map.end(); map_it++)
    {
        for (auto lst_it = (*map_it).second->begin(),
                     end = (*map_it).second->end(); lst_it != end; ++lst_it)
        {
            //DEBUG_PRINT((*lst_it)->data)
            //DEBUG_PRINT((*lst_it)->lst_ptr)
            delete[]((*lst_it)->data);
            //delete((*lst_it)->lst_ptr);
            delete(*lst_it);
        }
        //DEBUG_PRINT((*map_it).second)
        delete((*map_it).second);
    }
    path_lst_ptr_map.clear();
}

//**API Functions**//
/**
 * Description:
 * Params:
 * Return Value: a Non-negative integer indicating the number of bytes actually
 * read. Otherwise, the functions shall return -1 and set errno to indicate the
 * error.
 */
ssize_t read_cache(int fd, char* path, char *result_buf, size_t rounded_size_to_read, size_t offset_rounded)
//ssize_t read_from_cash(string path, int fd, char *result_buf, size_t size_to_read, off_t starting_offset, int numberOfBlocks, float fOld, float fNew)
{
    //Todo: These values should be dealt in the main.
    global_file_handle = fd;
    lst_new_cache_size = (unsigned int)(fNew * numberOfBlocks);
    lst_old_cache_size = (unsigned int)(fOld * numberOfBlocks);
    lst_mid_cache_size = numberOfBlocks - lst_new_cache_size - lst_old_cache_size;

    string str_path(path);

    ssize_t retstat = 0;  //ssize_t can also receive negative values for errors.
    size_t up_bound            = next_multiple(rounded_size_to_read, (size_t)blk_size);
    size_t offset_remainder    = (offset_rounded % (size_t) blk_size);
    size_t low_bound           = offset_rounded - offset_remainder;
    size_t buff_size = up_bound;

    //Todo: Again dont do this in the cache!!. the input is allways alligned!

    //Align the new memory container for the file.
    char *aligned_buf = (char*)memalign((size_t)blk_size, buff_size);

    //Searches for already saved blocks in memory, else read from fs.
    MAP_PATH_TO_BLOCK_PTR::iterator it = path_lst_ptr_map.find(str_path);
    if (it != path_lst_ptr_map.end()) //If the file's path is in cash.
    {
//        DEBUG_PRINT("Path been here.")
        retstat = retrive_all_info(str_path, aligned_buf, low_bound, up_bound);

    } else { //If it's a new fd.
//        DEBUG_PRINT("First tme path.")
        path_lst_ptr_map[str_path] = new list<node_info *>();
        retstat = retrive_all_info(str_path, aligned_buf, low_bound, up_bound);
    }

    //After getting all information to 'aligned_buf', copies only needed bytes
    //to 'result_buf'.
    if(retstat>=0)
    {
        //        dest    |          src
        memcpy(result_buf, &aligned_buf[offset_remainder], rounded_size_to_read);
        //retstat = strlen(result_buf);
    }
    if (retstat < 0) //In case of an error.
    {
        /* ltstat():
         * RETURN VALUES Upon successful completion a value of 0 is returned.
         * Otherwise, a value of -1 is returned and errno is set to indicate
         * the error.
         */
        retstat=-errno;
    }
    free(aligned_buf);
    return retstat;
}

void init_cache(int numberOfBlocks, __blksize_t blk_size, float fOld, float fNew)
{
    destroy_cache(); //For consecutive runs.
    ::numberOfBlocks = numberOfBlocks;
    ::blk_size = blk_size;
    ::fOld = fOld;
    ::fNew = fNew;
}

string debug_to_string()
{
    std::ostringstream stream;
    stream << "NEW List" << endl << "----------------" << endl;
    for (NEW_CACHE_PARTITION_LST::const_iterator it = lst_new_cache.begin(), end = lst_new_cache.end(); it != end; ++it)
    {
        stream << "[Type: " << (*it)->type << ", BlockNum:" << (*it)->block_num << ", RefCount:" << (*it)->ref_count << ", Data: " << ((*it)->data) << "], ";
    }

    stream << endl << "MID List" << endl << "----------------" << endl;
    for (NEW_CACHE_PARTITION_LST::const_iterator it = lst_mid_cache.begin(), end = lst_mid_cache.end(); it != end; ++it)
    {
        stream << "[Type: " << (*it)->type << ", BlockNum:" << (*it)->block_num << ", RefCount:" << (*it)->ref_count << ", Data: " << ((*it)->data) << "]" << "], ";
    }

    stream << endl << "OLD List" << endl << "----------------" << endl;
    for (NEW_CACHE_PARTITION_LST::const_iterator it = lst_old_cache.begin(), end = lst_old_cache.end(); it != end; ++it)
    {
        stream << "[Type: " << (*it)->type << ", BlockNum:" << (*it)->block_num << ", RefCount:" << (*it)->ref_count << ", Data: " << ((*it)->data) << "]" << "], ";
    }
    return stream.str();
}

/**
 * return value: npos (-1) means no matches were found,
 * else a a value >=0 will be returned.
 */
size_t change_cache_path(char* old_path, char *new_path)
{
    size_t matched_pos = string::npos;
    MAP_PATH_TO_BLOCK_PTR tmp_map;//Updating original map will result iterating over modified path again at the same function run.

    for(MAP_PATH_TO_BLOCK_ITR map_it = path_lst_ptr_map.begin(); map_it != path_lst_ptr_map.end(); map_it++)
    {
        string str_old(old_path), str_new(new_path), str_tmp;
        if((matched_pos = (map_it -> first.find(str_old))) != string::npos)
        {
            str_tmp.assign(map_it -> first);
            str_tmp.replace(matched_pos, str_old.length(), str_new);

            tmp_map[str_tmp] = path_lst_ptr_map[map_it -> first];
            path_lst_ptr_map.erase(map_it);

        } else
        {
            tmp_map[map_it -> first] = path_lst_ptr_map[map_it -> first];
            path_lst_ptr_map.erase(map_it);
        }
    }
    path_lst_ptr_map.insert(tmp_map.begin(), tmp_map.end());
    tmp_map.clear();
    return matched_pos;
}

string full_to_rel_path(string full_path)
{
    string root_dir; //Is global in fuse.
    

}

string cache_to_string()
{
    std::ostringstream stream;

    for (NEW_CACHE_PARTITION_LST::const_iterator it = lst_old_cache.begin(), end = lst_old_cache.end(); it != end; ++it)
    {
        stream << full_to_rel_path((*it)->path) << " ";
        stream << (*it)->block_num << " " << (*it)->ref_count << endl;
    }
    for (NEW_CACHE_PARTITION_LST::const_iterator it = lst_mid_cache.begin(), end = lst_mid_cache.end(); it != end; ++it)
    {
        stream << full_to_rel_path((*it)->path) << " ";
        stream << (*it)->block_num << " " << (*it)->ref_count << endl;
    }
    for (NEW_CACHE_PARTITION_LST::const_iterator it = lst_new_cache.begin(), end = lst_new_cache.end(); it != end; ++it)
    {
        stream << full_to_rel_path((*it)->path) << " ";
        stream << (*it)->block_num << " " << (*it)->ref_count << endl;
    }

    return stream.str();
}

//Todo: relpath, print ioctl,...