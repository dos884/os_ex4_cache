#include <iostream>
#include <fcntl.h>
#include <string.h>
#include "cache.h"
#include <sys/stat.h>
#include <unistd.h>
#include <cmath>

using namespace std;

//#define PATH "1"
#define PATH "../1"
#define BIG_PTH "../2"
#define slptm 1
#define BUF_SIZE 5000
#define SIZE_TO_READ 8
#define BLKSZ 4096

int cmp_bufs(char *buf1, char *buf2, int how_much) {
    for (int i = 0; i < how_much; ++i) {
        if (buf1[i] != buf2[i]) {
            return -i;
        }
    }
    return 1;
}

void check(char *path, int fd, int fd_for_cache, size_t size_to_read,
           size_t offset, int blksz) {
    char* result_buf=(char*)malloc(sizeof(char)*size_to_read);
    char* result_buf_naive=(char*)malloc(sizeof(char)*size_to_read);
    ssize_t result_cch = read_cache(fd_for_cache, path, result_buf, size_to_read, offset);
    ssize_t resuly_naive = pread(fd, result_buf_naive, size_to_read, offset);
    cout << result_cch << " : " << resuly_naive << endl;
    if (result_cch == resuly_naive) {
            int cmp = cmp_bufs(result_buf_naive, result_buf, resuly_naive);
            if (cmp != 1) {
                cout <<"ERROR!!!\n"<< "BUFF NEQ AT: " << -cmp << endl;
                DEBUG_PRINT("Buf Pread: " << result_buf_naive << endl << "Buf cch: " << result_buf << endl)
                usleep(slptm);
            } else {
                cout << "OK\n BUFS EQ till" << resuly_naive<< endl;
            }
        } else {
            cout <<"ERROR!!!\n"<< "results not eqוal" << endl; /*<< result_buf << endl << result_buf_naive << endl;*/
            int cmp = cmp_bufs(result_buf_naive, result_buf, max(resuly_naive,result_cch));
            if (cmp != 1) {
                cout << "BUFF NEQ AT: " << -cmp << endl;
            } else {
                cout << " BUFS EQ till: " << max(resuly_naive, result_cch) << endl;
            }
            usleep(slptm);
        }
    cout << "---------------------" << endl;
    free(result_buf);
    free(result_buf_naive);
}

int main() {
//    int result_cch, resuly_naive;
//    char *path = new char[strlen(PATH) + 1];
//    strcpy(path, PATH);

    char path[] = PATH;

    int fd = open(path, O_RDONLY);
    int fd_for_cache = open(path, O_RDONLY);
    if (fd < 0 || fd_for_cache<0) {
        cout << "Failed opening the file. err: " << errno << endl;
        exit(-1);
    }
    //Find machine block size and store it globally.
    struct stat fi;
    stat(path, &fi);
    cout << "System block size: " << fi.st_blksize << endl;


    cout << " RANDOM TEST \n";
    size_t size_to_read = 0;
    size_t offset = 0;
    int cur_blksz = 3;
//
//    init_cache(100, cur_blksz, 0.33, 0.33);
//    for (int i = 0; i < 100; ++i) {
//        size_to_read = i * cur_blksz;
//        check(path, fd, fd_for_cache, size_to_read, offset, cur_blksz);
//    }
//    destroy_cache();


    init_cache(10000, 100, 0.33, 0.33);
    check(path, fd, fd_for_cache, 100 , 0, cur_blksz);
    destroy_cache();
//
//    cur_blksz=5;
//    init_cache(10000, cur_blksz, 0.33, 0.33);
//    for (int i = 0; i < 13; ++i) {
//
//        size_to_read = 100 * cur_blksz;
//        //size_to_read = i * cur_blksz;
//        offset=pow(2,i)*cur_blksz;
//        check(path, fd, fd_for_cache, size_to_read, offset, cur_blksz);
//    }
//    change_cache_path(".", "Pilllll");
//    destroy_cache();
}

//#include <iostream>
//#include <fcntl.h>
//#include <string.h>
//#include "cache.h"
//#include <sys/stat.h>
//
//using namespace std;
//
//#define PATH "/cs/stud/asafbe/safe/OS/os_ex4_naive/bin/Test_Files/Test1"
//#define BUF_SIZE 5000
//#define SIZE_TO_READ 44
//#define BLKSZ 4096
//
//int main()
//{
//    int result;
//    char *path = new char [strlen(PATH) + 1];
//    strcpy(path, PATH);
//    int fd = open(PATH, O_RDONLY);
//    size_t rounded_size_to_read = SIZE_TO_READ;
//    char *result_buf = new char[sizeof(char) * BUF_SIZE];
//    off_t offset_rounded = 2;
//    int numberOfBlocks = 10;
//    float fOld = 0.33;
//    float fNew = 0.33;
//    __blksize_t blk_size;
//
//    //Find machine block size and store it globally.
//    struct stat fi;
//    stat("/tmp", &fi);
//    blk_size = fi.st_blksize;
//
//    blk_size = 6;
//
//    init_cache(numberOfBlocks, blk_size, fOld, fNew);
//
//    if(fd >= 0)
//    {
////        result = read_from_cash(*path, fd, result_buf, size_to_read, starting_offset, numberOfBlocks, fOld, fNew);
//        result = read_cache(fd, path, result_buf, rounded_size_to_read, offset_rounded);
//        result = read_cache(fd, path, result_buf, rounded_size_to_read, offset_rounded);
//    } else
//    {
//        DEBUG_PRINT("Failed opening the file.")
//    }
//
//    DEBUG_PRINT("Output from buffer: " << result_buf)
//
//    delete[](result_buf);
//    if(result >= 0)
//    {
//        return 0;
//    } else
//    {
//        return (-1);
//    }
//}