//
// Created by asafbe on 5/20/16.
//

#ifndef CACHING_CACH_H
#define CACHING_CACH_H

#include <glob.h>
#include <stdio.h>
#include <string>

#define DEBUG_PRINT(str)  \
std::cout << str << std::endl;

using namespace std;

//TODO : kill this functions.
//void int read_from_cash(char* path , char *result_buf, size_t size, off_t offset);
//ssize_t read_from_cash(string path, int fd, char *result_buf, size_t size_to_read, off_t starting_offset, int numberOfBlocks, float fOld, float fNew);


/**
 * This is the main cache reading function.
 * The cache holds internally a map of <fd:full_path> and a map of <full_path:existing_blocks>
 * Assumption: the size and offset are rounded!
 */
ssize_t read_cache(int fd, char*path, char *result_buf, size_t rounded_size_to_read, size_t offset_rounded);

/**
 * Links all existing blocks of a certain path, to a new path.
 * The old path is then removed from the mapping.
 * When giving a partial path, that fits several records, changes that part in
 * all of the paths.
 * Returns: bool value for success or failure.
 */
size_t change_cache_path(char* old_path, char *new_path);

/**
 * Init the cache only once!
 */
void init_cache(int numberOfBlocks, __blksize_t blk_size, float fOld, float fNew);

/**
 * Destroys the cache
 */
void destroy_cache();

/**
 * outputs cache to a string representation. might be slow dont use in performance critical code
 */
string cache_to_string();


#endif //CACHING_CACH_H
